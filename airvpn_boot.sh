#!/bin/bash
# script to connect to recommended AirVPN server, created to be used in systemd unit at boot
# This is the script that tries to establish a connection at boot.
# default location of this file: "/usr/local/bin/airvpn_boot.sh"

# check if necessary programs are installed
PROGRAMS=( hummingbird curl )
MISSING="false"
for p in "${PROGRAMS[@]}"
do
	command -v $p $> /dev/null
	if [ ! $? = "0" ]
	then
		MISSING="true"
	fi
done
if [ "$MISSING" = "true" ]
then
	exit
fi

# check which network filter is available (determined NETFILTER will be overriden if set in config file)
NETFILTERS_AVAILABLE=( iptables iptables-legacy nft )
NETFILTER="none"
for n in "${NETFILTERS_AVAILABLE[@]}"
do
	command -v $n $> /dev/null
	if [ $? = "0" ]
	then
		NETFILTER="$n"
		break
	fi
done

# source variables which are subject to change from config file
source "/home/<USER>/.vpncontrol/config/vpncontrol.conf"

# set network-lock argument for hummingbird depending on available backends
if [ "$NETFILTER" = "nft" ]
then
	NETFILTER_HUM="nftables"
elif [ "$NETFILTER" = "iptables" -o "$NETFILTER" = "iptables-legacy" ]
then
	NETFILTER_HUM="iptables"
elif [ "$NETFILTER" = "off" ]
then
	NETFILTER_HUM="off"
else
	NETFILTER_HUM="on"
fi

# in order to correctly send notifications via notify-send as root, DISPLAY variable must be set (only on X, not on Wayland) and DBUS_SESSION_BUS_ADDRESS (automatically set based on username)
#DISPLAY=:0
USER_ID=$( id -u $SCRIPT_USER )
DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$USER_ID/bus"

function activate_lock {
	# use detour with cat because SELinux denies direct read/write access for iptables and nft (actually nft was only denied write access, couldn't read for other permission reason, but this way it works)
	if [ "$NETFILTER" = "iptables-legacy" ]
	then
		iptables-legacy-save | cat  > "${NETFILTER_RULES_IPTABLES}ipv4.backup"
		ip6tables-legacy-save | cat > "${NETFILTER_RULES_IPTABLES}ipv6.backup"
		cat "${NETFILTER_RULES_IPTABLES}ipv4" | iptables-legacy-restore
		cat "${NETFILTER_RULES_IPTABLES}ipv6" | ip6tables-legacy-restore
	elif [ "$NETFILTER" = "iptables" ]
	then
		iptables-save | cat  > "${NETFILTER_RULES_IPTABLES}ipv4.backup"
		ip6tables-save | cat > "${NETFILTER_RULES_IPTABLES}ipv6.backup"
		cat "${NETFILTER_RULES_IPTABLES}ipv4" | iptables-restore
		cat "${NETFILTER_RULES_IPTABLES}ipv6" | ip6tables-restore
	elif [ "$NETFILTER" = "nft" ]
	then
		# put command to flush ruleset at top of backup file, so when it is loaded to restore the old rules, all previous rules are deleted in the same transaction (would take 2 transacions otherwise)
		echo "flush ruleset" > "${NETFILTER_RULES_NFTABLES}.backup"
		nft list ruleset  | cat >> "${NETFILTER_RULES_NFTABLES}.backup"
		cat "${NETFILTER_RULES_NFTABLES}" | nft -f -
	fi
}

function connect_server {
		# write leftover log files to correct locations
		LOGS_CURRENT_OLD=($( ls "$LOG_PATH" | grep hummingbird_current.*log ))
		if [ ${#LOGS_CURRENT_OLD[@]} -gt 0 ]
		then
			for i in "${LOGS_CURRENT_OLD[@]}"
			do
				cat "$LOG_PATH/$i" >> "$LOG_PATH/${i/_current/}"
				rm "$LOG_PATH/$i" 
			done
		fi

		DATE=$( date +%Y%m%d )

		# names and number of currently present logs
		LOG_NAMES=($( ls "$LOG_PATH" | grep hummingbird.*log | sort -d ))
		LOG_NR=${#LOG_NAMES[@]}

		# if no log files should be kept, discard current logfile after process finishes, otherwise append to log file of current date
		if [ "$LOG_DAYS" = "0" ]
		then
			LOG_FINISH="/dev/null"
		else
			LOG_FINISH="$LOG_PATH/hummingbird_$DATE.log"
		fi

		# check if newest log file is from today and if not, increase counter, so with the upcoming logfile the file limit will be kept and create final log file as user, so the user can write to it later
		if [ "$LOG_NR" -gt "0" ]
		then
			if [ ! $( echo ${LOG_NAMES[-1]/#hummingbird_/} | cut -d "." -f 1 ) = "$DATE" ]
			then
				LOG_NR=$(( $LOG_NR+1 ))
				if [ ! "$LOG_DAYS" = "0" ]
				then
					su "$SCRIPT_USER" -c "touch $LOG_FINISH"
				fi
			fi

			# check if more logs (including the upcoming one) are present than there should be and if so, remove oldest ones
			if [ "$LOG_NR" -gt "$LOG_DAYS" ]
			then
				cd "$LOG_PATH"
				rm "${LOG_NAMES[@]:0:(( $LOG_NR-$LOG_DAYS ))}"
				cd -
			fi
		else
			su "$SCRIPT_USER" -c "touch $LOG_FINISH"
		fi

		su "$SCRIPT_USER" -c "notify-send 'AirVPN' 'Connecting to AirVPN ...'"

		# run hummingbird in background (and send notification when process finishes), pipe output to log
		(hummingbird $HUM_OPTIONS --network-lock "$NETFILTER_HUM" --timeout "$TIMEOUT_REC" --server "$1".vpn.airdns.org "$CONFIG_PATH" &>> "$LOG_PATH/hummingbird_current_$DATE.log"; su "$SCRIPT_USER" -c "notify-send.sh 'AirVPN' 'Hummingbird process has finished.'"; sleep 1; cat "$LOG_PATH/hummingbird_current_$DATE.log" >> "$LOG_FINISH"; rm "$LOG_PATH/hummingbird_current_$DATE.log") &
		# monitor log to catch sign of successful connection
		tail -f -n 5 "$LOG_PATH/hummingbird_current_$DATE.log" | timeout --signal=SIGINT "$TIMEOUT_CON" grep -q -m 1 "EVENT: CONNECTED"
		INIT_EXIT=$?
		pkill -f tail.*hummingbird_current

		if [ "$INIT_EXIT" = "0" ]
		then
			# send notification as regular user for it to be sent and displayed correctly
			su "$SCRIPT_USER" -c "notify-send 'AirVPN' 'VPN connection successfully established.'"
			exit
		else
			pkill -2 hummingbird
			su "$SCRIPT_USER" -c "notify-send 'AirVPN' 'Connection attempt to an AirVPN server has failed.'"
			# need to wait long enough, so "current" log file is deleted before next connection attempt, otherwise file counter will be too high and delete other log files (takes around +20ms, but sometimes more, so better to add 1s)
			sleep 2
		fi


}

INIT_EXIT="1"

if [ "$DEFAULT_NETLOCK" = "enabled" ]
then
	activate_lock
fi

# try to connect to recommended servers (first EU, then rest of the world; change order/adjust server lists if desired)
connect_server "earth"
if [ ! "$INIT_EXIT" = "0" ]
then
	# count connection attempts in order to stop after certain number
	count="1"
	for s in "${SERVERS_BEST_EU[@]}"
	do
		connect_server "$s"
		if [ $INIT_EXIT = "0" ]
		then
			break
		else
			(( count++ ))
		fi
		if [ "$count" -ge 5 -o "$count" -ge "$CON_ATTEMPTS" ]
		then
			if [ "$count" -ge "$CON_ATTEMPTS" ]
			then
				su "$SCRIPT_USER" -c "notify-send 'AirVPN' 'Connection unsuccessful after '$count' failed attempts."
			fi
			break
		fi
	done
fi
if [ ! "$INIT_EXIT" = "0" -a "$count" -lt "$CON_ATTEMPTS" ]
then
	for s in "${SERVERS_BEST_REST[@]}"
	do
		connect_server "$s"
		if [ $INIT_EXIT = "0" ]
		then
			break
		else
			(( count++ ))
		fi
		if [ "$count" -ge "$CON_ATTEMPTS" ]
		then
			su "$SCRIPT_USER" -c "notify-send 'AirVPN' 'Connection unsuccessful after '$count' failed attempts."
			break
		fi
	done
fi
exit
