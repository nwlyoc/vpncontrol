#!/bin/bash
# interactive shell script to control the command line version of the AirVPN Hummingbird client and openconnect more comfortably and extensively
# originally created in January 2019 for Eddie, updated for use with Hummingbird in June 2020
# This is the main script, the interface.
# default location of this file: "$HOME/.local/bin/VPNControl.sh"

# check if at least bash 4 is used
if [ "${BASH_VERSINFO[0]}" -lt "4" ]
then
	echo "This sript can only be run with bash 4 or higher."
	exit
fi

# check if necessary programs are installed
PROGRAMS=( hummingbird dialog curl awk )
MISSING="false"
for p in "${PROGRAMS[@]}"
do
	command -v $p $> /dev/null
	if [ ! $? = "0" ]
	then
		echo "Please install $p to use this script."
		MISSING="true"
	fi
done
if [ "$MISSING" = "true" ]
then
	exit
fi

# check which network filter is available (determined NETFILTER will be overriden if set in config file)
NETFILTERS_AVAILABLE=( iptables iptables-legacy nft )
NETFILTER="none"
for n in "${NETFILTERS_AVAILABLE[@]}"
do
	command -v $n $> /dev/null
	if [ $? = "0" ]
	then
		NETFILTER="$n"
		break
	fi
done

# source variables which are subject to change from config file
VPNCONTROL_CONFIG="$HOME/.vpncontrol/config/vpncontrol.conf"
source "$VPNCONTROL_CONFIG"

# variables which probably won't have to be changed
DIALOG_OK=0
DIALOG_CANCEL=1
DIALOG_HELP=2
DIALOG_EXTRA=3
DIALOG_ITEM_HELP=4
DIALOG_ESC=255
HEIGHT=0
WIDTH=0
BACKTITLE="VPN Control"
FORMAT="text"
URL="https://airvpn.org/api/"
COLS=$( tput cols )
ROWS=$( tput lines )
PID=$$
# set network-lock argument for hummingbird depending on available backends
if [ "$NETFILTER" = "nft" ]
then
	NETFILTER_HUM="nftables"
elif [ "$NETFILTER" = "iptables" -o "$NETFILTER" = "iptables-legacy" ]
then
	NETFILTER_HUM="iptables"
elif [ "$NETFILTER" = "off" ]
then
	NETFILTER_HUM="off"
else
	NETFILTER_HUM="on"
fi

function check_sudo {
# check if user has sudo privileges
sudo -vn &> /dev/null
# gain sudo privileges for commands that need it (better than running everything with sudo)
if [ $? = "1" ]
then
	unset EXIT_STATUS_SUDO
	#PASS_PROMPT="Establishing OpenVPN connections and checking and changing network traffic rules requires root privileges. Please enter your password:"
	until [ "$EXIT_STATUS_SUDO" = "0" ]
	do
		dialog \
		--backtitle "$BACKTITLE" \
		--title "Password Needed" \
		--output-fd 1 \
		--insecure \
		--passwordbox "$PASS_PROMPT" 11 35 | xargs printf '%s\n' | sudo -Svp '' &> /dev/null
		EXIT_STATUS_PIPE=( "${PIPESTATUS[@]}" )
		EXIT_STATUS_DIALOG="${EXIT_STATUS_PIPE[0]}"
		EXIT_STATUS_SUDO="${EXIT_STATUS_PIPE[2]}"
		EXIT_SUDO_TEST="${EXIT_STATUS_PIPE[2]}"
		PASS_PROMPT="The password you entered is incorrect. Please try again:"
		case $EXIT_STATUS_DIALOG in
			$DIALOG_CANCEL|$DIALOG_ESC)
				return 1
				;;
		esac
	done
	# keep sudo permission until script exits or permissions are revoked (e.g. when computer goes to sleep)
	while [ "$EXIT_SUDO_TEST" = "0" ]; do sudo -vn; EXIT_SUDO_TEST=$?; sleep 60; kill -0 "$PID" || exit; done &> /dev/null &
fi

return 0
}

function get_list {
	SERVICE_NAME="status"
	timeout --signal=SIGINT 10 curl -s "$URL$SERVICE_NAME/?format=$FORMAT" > "/tmp/.airvpn_server_list.txt"
}

function sort_list_servers {
	# pipe server status list to awk, filter out unnecessary stuff,
	# combine lines that relate to same server into single lines which are saved as array,
	# loop through array to format info,
	# print array and sort according to options,
	# add numbers to list for menu
	LIST_SERVERS=$(awk -F '[.]' \
		'BEGIN{OFS=";"} \
		/^servers/ && !/ip_/ && !/country_code/ {c=$2; \
		if (c in servers) servers[c]=servers[c] OFS $3; \
		else servers[c]=$3; \
		for (k in servers) gsub(/;bw=/, "  :", servers[k]); \
		for (k in servers) gsub(/;bw_max=/, "/", servers[k]); \
		for (k in servers) gsub(/;currentload=/, "  :", servers[k]); \
		for (k in servers) gsub(/;health=/, "%:", servers[k]); \
		for (k in servers) gsub(/;.*=/, ":", servers[k]); \
		for (k in servers) gsub(/^.*=/, "", servers[k])} \
		END{for (c in servers) print servers[c]}' "/tmp/.airvpn_server_list.txt" | sort -t ":" $1)
		LIST_SERVERS=$( echo "$LIST_SERVERS" | sed 's/:/;/' )
}

function sort_list_countries {
	LIST_COUNTRIES=$(awk -F '[.]' \
		'BEGIN{OFS=";"} \
		/^countries/ && (/country_name/ || /country_code/) {c=$2; \
		if (c in countries) countries[c]=countries[c] OFS $3; \
		else countries[c]=$3; \
		for (k in countries) gsub(/;.*=/, ":", countries[k]); \
		for (k in countries) gsub(/^.*=/, "", countries[k])} \
		END{for (c in countries) print countries[c]}' "/tmp/.airvpn_server_list.txt" | sort -t ":" -d)
}

function get_userinfo {
	pgrep hummingbird &> /dev/null
	if [ $? = 0 ]
	then
		SERVICE_NAME="userinfo"
		# filter specific lines, save values (after "=") to variables after protecting whitespace
		read U_LOGIN U_EXP U_CONNECTED U_DEVICE U_SERVER_NAME U_SERVER_COUNTRY U_SERVER_LOCATION U_TIME <<< $( \
		timeout --signal=SIGINT 3 curl -s "$URL$SERVICE_NAME/?key=$API_KEY&format=$FORMAT" | \
		awk -F '[=]' \
			'BEGIN{ORS=";"} \
			/^user.login|^user.expiration_days|^user.connected|^sessions.*device_name|^connection.server_name|^connection.server_country=|^connection.server_location|^connection.connected_since_date/ \
			{print $2}' | \
		sed 's/\ /\\\ /g' | sed 's/;/\ /g' \
		)
		if [ "$U_CONNECTED" = "true" ]
		then
			U_CONNECTED="connected"
			U_SERVER_FULL="$U_SERVER_NAME ($U_SERVER_LOCATION, $U_SERVER_COUNTRY)"
			U_TIME=$(date -d "$U_TIME UTC" +"%a %d. %b %Y %H:%M:%S")
		else
			U_CONNECTED="not connected (connection timeout)"
			U_SERVER_FULL="--"
			U_TIME="--"
		fi
	else
		U_CONNECTED="not connected (Hummingbird not running)"
		U_SERVER_FULL="--"
		U_TIME="--"
	fi
}

function connect_server {
	if [ "$KILLED" = "true" ]
	then

		# write leftover log files to correct locations
		LOGS_CURRENT_OLD=($( ls "$LOG_PATH" | grep hummingbird_current.*log ))
		if [ ${#LOGS_CURRENT_OLD[@]} -gt 0 ]
		then
			for i in "${LOGS_CURRENT_OLD[@]}"
			do
				cat "$LOG_PATH/$i" >> "$LOG_PATH/${i/_current/}"
				rm -f "$LOG_PATH/$i" 
			done
		fi

		DATE=$( date +%Y%m%d )
		LOG_NAMES=($( ls "$LOG_PATH" | grep hummingbird.*log | sort -d ))
		LOG_NR=${#LOG_NAMES[@]}

		LOG_CURRENT="$LOG_PATH/hummingbird_current_$DATE.log"
		# if no log files should be kept, discard current logfile after process finishes, otherwise append to log file of current date
		if [ "$LOG_DAYS" = "0" ]
		then
			LOG_FINISH="/dev/null"
		else
			LOG_FINISH="$LOG_PATH/hummingbird_$DATE.log"
		fi

		if [ "$LOG_NR" -gt "0" ]
		then
			# check if newest log file is from today and if not, increase counter, so with the upcoming logfile the file limit will be kept
			if [ ! $( echo ${LOG_NAMES[-1]/#hummingbird_/} | cut -d "." -f 1 ) = "$DATE" ]
			then
				LOG_NR=$(( $LOG_NR+1 ))
			fi

			# check if more logs (including the upcoming one) are present than there should be and if so, remove oldest ones
			if [ "$LOG_NR" -gt "$LOG_DAYS" ]
			then
				cd "$LOG_PATH"
				rm -f "${LOG_NAMES[@]:0:(( $LOG_NR-$LOG_DAYS ))}"
				cd -
			fi
		fi

		# run hummingbird in background and detached from current window, write output to logfile, read it from there to dialog and catch sign of successful connection
		# hummingbird's timeout option is used, so it has enough time after sleep to recover without trying forever; TIMEOUT variable is used to try another server after some time when it is reasonable to not expect a successful connection anymore
		(sudo hummingbird $HUM_OPTIONS --network-lock "$NETFILTER_HUM" --timeout "$TIMEOUT_REC" --server "$1"."$DOMAIN" "$CONFIG_PATH" &> "$LOG_CURRENT"; notify-send "AirVPN" "Hummingbird process has finished."; sleep 1; cat "$LOG_CURRENT" >> "$LOG_FINISH"; rm -f "$LOG_CURRENT") & 
		tail -f -n 5 "$LOG_CURRENT"  | dialog --backtitle "$BACKTITLE" --title "Connecting to AirVPN  (Server: $1) ..." --progressbox 20 80 &
		tail -f -n 5 "$LOG_CURRENT"  | timeout --signal=SIGINT "$TIMEOUT_CON" grep -q -m 1 "EVENT: CONNECTED"
		INIT_EXIT=$?
		pkill -f tail.*hummingbird_current
		if [ "$INIT_EXIT" = "0" ]
		then
			sleep 1
			get_userinfo
			notify-send "AirVPN" "VPN connection successfully established to AirVPN's server $U_SERVER_FULL."
		else
			U_CONNECTED="error during connection attempt"
			U_SERVER_FULL="--"
			U_TIME="--"
			sudo pkill -2 hummingbird
			notify-send "AirVPN" "Connection attempt to an AirVPN server has failed."
			# need to wait long enough, so "current" log file is deleted before next connection attempt, otherwise file counter will be too high and delete other log files (takes arount +20ms, but sometimes more, so better to add 1s)
			sleep 2
		fi
	else
		U_CONNECTED="error during disconnection"
		U_SERVER_FULL="--"
		U_TIME="--"
	fi
}

function connect_openconnect {
	if [ "$KILLED" = "true" ]
	then
		DATE=$( date +%Y%m%d )
		LOG_NAMES=($( ls "$LOG_PATH" | grep openconnect.*log | sort -d ))
		LOG_NR=${#LOG_NAMES[@]}

		# if no log files should be kept, discard current logfile after process finishes, otherwise append to log file of current date
		if [ "$LOG_DAYS" = "0" ]
		then
			LOG_FINISH="/dev/null"
		else
			LOG_FINISH="$LOG_PATH/openconnect_$DATE.log"
		fi

		if [ "$LOG_NR" -gt "0" ]
		then
			# check if newest log file is from today and if not, increase counter, so with the upcoming logfile the file limit will be kept
			if [ ! $( echo ${LOG_NAMES[-1]/#openconnect_/} | cut -d "." -f 1 ) = "$DATE" ]
			then
				LOG_NR=$(( $LOG_NR+1 ))
			fi

			# check if more logs (including the upcoming one) are present than there should be and if so, remove oldest ones
			if [ "$LOG_NR" -gt "$LOG_DAYS" ]
			then
				cd "$LOG_PATH"
				rm -f "${LOG_NAMES[@]:0:(( $LOG_NR-$LOG_DAYS ))}"
				cd -
			fi
		fi

		ALT_SERVER=$(echo -n "$CONNECT_INFO" | cut -d$'\n' -f 1)
		ALT_GROUP=$(echo -n "$CONNECT_INFO" | cut -d$'\n' -f 2)
		ALT_USER=$(echo -n "$CONNECT_INFO" | cut -d$'\n' -f 3)
		ALT_PASS=$(echo -n "$CONNECT_INFO" | cut -d$'\n' -f 4)
		ALT_OPTS=$(echo -n "$CONNECT_INFO" | cut -d$'\n' -f 5)
		echo "$ALT_PASS" | (sudo openconnect $ALT_OPTS --authgroup=$ALT_GROUP --user=$ALT_USER --passwd-on-stdin $ALT_SERVER &> "$LOG_PATH/openconnect_current_$DATE.log"; notify-send "Openconnect" "Openconnect process has finished."; sleep 1; cat "$LOG_PATH/openconnect_current_$DATE.log" >> "$LOG_FINISH"; rm -f "$LOG_PATH/openconnect_current_$DATE.log") & 
		timeout --signal=SIGINT 3 tail -f -n 20 "$LOG_PATH/openconnect_current_$DATE.log" | dialog --backtitle "$BACKTITLE" --title "Connecting via openconnect ..." --timeout 5 --programbox 20 80
		U_CONNECTED="connected"
		U_SERVER_FULL="$ALT_SERVER"
		U_TIME=$(date +"%a %d. %b %Y %H:%M:%S")
	else
		U_CONNECTED="error during disconnection"
		U_SERVER_FULL="--"
		U_TIME="--"
	fi
}

function disconnect_server {
	# check for running instance of hummingbird
	HUM_PID=$( pgrep hummingbird )
	if [ $? = 0 ]
	then
		# kill process and wait for confirmation from process output
		# check if running instance of hummingbird is writing to logfile and if so, listen there for confirmation
		sudo ls -l "/proc/$HUM_PID/fd" | grep hummingbird_current &> /dev/null
		if [ $? = 0 ]
		then
			sudo pkill -2 hummingbird &
			tail -f -n 5 "$LOG_PATH/hummingbird_current_"* | dialog --backtitle "$BACKTITLE" --title "Disconnecting from AirVPN ..." --progressbox 20 80 &
			tail -f -n 5 "$LOG_PATH/hummingbird_current_"* | timeout --signal=SIGINT 3 grep -q -m 1 "Thread finished"
			pkill -f tail.*hummingbird_current
		else
			# in case connection was started without this script
			sudo pkill -2 hummingbird
			sleep 2
		fi
		# give some time to completely close process, without sleep it's too early for new connection
		sleep 1
		pgrep hummingbird &> /dev/null
		if [ $? = 1 ]
		then
			KILLED1="true"
			notify-send "AirVPN" "VPN connection has been stopped successfully."
		else
			KILLED1="false"
			notify-send "AirVPN" "An error has occured during the disconnection attempt."
		fi
	else
		KILLED1="true"
	fi

	# check for running instance of openconnect
	pgrep -f "openconnect.*--" &> /dev/null
	if [ $? = 0 ]
	then
		pkill -2 -f "openconnect.*--"
		sleep 1
		pgrep -f "openconnect.*--" &> /dev/null
		if [ $? = 1 ]
		then
			KILLED2="true"
			notify-send "AirVPN" "VPN connection to openconnect has been stopped successfully."
			# somehow openconnect doesn't receive SIGINT and shuts down improperly,
			# so vpnc can't restore resolv.conf by itself
			sudo cp "/var/run/vpnc/resolv.conf-backup" "/etc/resolv.conf"
		else
			KILLED2="false"
			notify-send "AirVPN" "An error has occured during the attempt to disconnect from openconnect."
		fi
	else
		KILLED2="true"
	fi

	if [ "$KILLED1" = "true" -a "$KILLED2" = "true" ]
	then
		KILLED="true"
	else
		KILLED="false"
	fi
}

function toggle_lock {
	if [ "$1" = "activate" ]
	then
		if [ "$NETFILTER" = "iptables-legacy" ]
		then
			sudo iptables-legacy-save  > "${NETFILTER_RULES_IPTABLES}ipv4.backup"
			sudo ip6tables-legacy-save > "${NETFILTER_RULES_IPTABLES}ipv6.backup"
			sudo iptables-legacy-restore < "${NETFILTER_RULES_IPTABLES}ipv4"
			sudo ip6tables-legacy-restore < "${NETFILTER_RULES_IPTABLES}ipv6"
		elif [ "$NETFILTER" = "iptables" ]
		then
			sudo iptables-save  > "${NETFILTER_RULES_IPTABLES}ipv4.backup"
			sudo ip6tables-save > "${NETFILTER_RULES_IPTABLES}ipv6.backup"
			sudo iptables-restore < "${NETFILTER_RULES_IPTABLES}ipv4"
			sudo ip6tables-restore < "${NETFILTER_RULES_IPTABLES}ipv6"
		elif [ "$NETFILTER" = "nft" ]
		then
			# put command to flush ruleset at top of backup file, so when it is loaded to restore the old rules, all previous rules are deleted in the same transaction (would take 2 transacions otherwise)
			echo "flush ruleset" > "${NETFILTER_RULES_NFTABLES}.backup"
			sudo nft list ruleset  >> "${NETFILTER_RULES_NFTABLES}.backup"
			sudo nft -f "${NETFILTER_RULES_NFTABLES}"
		fi
	elif [ "$1" = "deactivate" ]
	then
		if [ "$NETFILTER" = "iptables-legacy" ]
		then
			if [ -s "${NETFILTER_RULES_IPTABLES}ipv4.backup" ]
			then
				sudo iptables-legacy-restore < "${NETFILTER_RULES_IPTABLES}ipv4.backup"
				sudo rm -f "${NETFILTER_RULES_IPTABLES}ipv4.backup"
			else
				sudo iptables-legacy -F
				sudo iptables-legacy -t nat -F
			fi
			if [ -s "${NETFILTER_RULES_IPTABLES}ipv6.backup" ]
			then
				sudo ip6tables-legacy-restore < "${NETFILTER_RULES_IPTABLES}ipv6.backup"
				sudo rm -f "${NETFILTER_RULES_IPTABLES}ipv6.backup"
			else
				sudo ip6tables-legacy -F
				sudo ip6tables-legacy -t nat -F
			fi
		elif [ "$NETFILTER" = "iptables" ]
		then
			if [ -s "${NETFILTER_RULES_IPTABLES}ipv4.backup" ]
			then
				sudo iptables-restore < "${NETFILTER_RULES_IPTABLES}ipv4.backup"
				sudo rm -f "${NETFILTER_RULES_IPTABLES}ipv4.backup"
			else
				sudo iptables -F
				sudo iptables -t nat -F
			fi
			if [ -s "${NETFILTER_RULES_IPTABLES}ipv6.backup" ]
			then
				sudo ip6tables-restore < "${NETFILTER_RULES_IPTABLES}ipv6.backup"
				sudo rm -f "${NETFILTER_RULES_IPTABLES}ipv6.backup"
			else
				sudo ip6tables -F
				sudo ip6tables -t nat -F
			fi
		elif [ "$NETFILTER" = "nft" ]
		then
			if [ -s "${NETFILTER_RULES_NFTABLES}.backup" ]
			then
				sudo nft -f "${NETFILTER_RULES_NFTABLES}.backup"
				sudo rm -f "${NETFILTER_RULES_NFTABLES}.backup"
			else
				sudo nft flush ruleset
			fi
		fi
	else
		return 1
	fi

	check_lock
	if [ "$LOCK_ACTIVE" = "inactive" ]
	then
		dialog --backtitle "$BACKTITLE" --title "Default Network Lock Inactive" --msgbox "$MISSINGRULES" $HEIGHT $WIDTH
	elif [ "$LOCK_ACTIVE" = "active" ]
	then
		dialog --backtitle "$BACKTITLE" --title "Default Network Lock Active" --timeout 3 --msgbox "The default network lock is active." $HEIGHT $WIDTH
	else
		return 1
	fi
}

function check_lock {
	if [ "$NETFILTER" = "iptables-legacy" ]
	then
		# load rules from ruleset file into array (only -A rules (append) are loaded), prefix with iptables or ip6tables command as fitting, change -A (append) to -C (check)
		mapfile -t IPRULESIPV4 < <(grep -e "-A " "${NETFILTER_RULES_IPTABLES}ipv4" | sed -e 's/^\(.*\)/sudo iptables-legacy \1/' -e 's/\ -A / -C /')
		mapfile -t IPRULESIPV6 < <(grep -e "-A " "${NETFILTER_RULES_IPTABLES}ipv6" | sed -e 's/^\(.*\)/sudo ip6tables-legacy \1/' -e 's/\ -A / -C /')
		LOCK_ACTIVE="error while checking"
		# only checks for presence of rules, not for order; if not present in default table (filter), check in other tables
		MISSINGRULES="The following rules are not present:\n"
		for i in "${IPRULESIPV4[@]}"
		do
			eval "$i" &> /dev/null
			if [ ! $? = "0" ]
			then
				eval "${i/legacy/legacy -t nat}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				eval "${i/legacy/legacy -t mangle}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				eval "${i/legacy/legacy -t raw}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				eval "${i/legacy/legacy -t security}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				MISSINGRULES="$MISSINGRULES\nIPv4: $i"
				LOCK_ACTIVE="inactive"
			fi
		done
		for i in "${IPRULESIPV6[@]}"
		do
			eval "$i" &> /dev/null
			if [ ! $? = "0" ]
			then
				eval "${i/legacy/legacy -t nat}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				eval "${i/legacy/legacy -t mangle}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				eval "${i/legacy/legacy -t raw}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				eval "${i/legacy/legacy -t security}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				MISSINGRULES="$MISSINGRULES\nIPv6: $i"
				LOCK_ACTIVE="inactive"
			fi
		done
		
		if [ "$LOCK_ACTIVE" = "inactive" ]
		then
			MISSINGRULES="${MISSINGRULES//sudo iptables -C /}\n\nPlease check manually."
			MISSINGRULES="${MISSINGRULES//sudo ip6tables -C /}"
		else
			LOCK_ACTIVE="active"
		fi
	elif [ "$NETFILTER" = "iptables" ]
	then
		# load rules from ruleset file into array (only -A rules (append) are loaded), prefix with iptables or ip6tables command as fitting, change -A (append) to -C (check)
		mapfile -t IPRULESIPV4 < <(grep -e "-A " "${NETFILTER_RULES_IPTABLES}ipv4" | sed -e 's/^\(.*\)/sudo iptables \1/' -e 's/\ -A / -C /')
		mapfile -t IPRULESIPV6 < <(grep -e "-A " "${NETFILTER_RULES_IPTABLES}ipv6" | sed -e 's/^\(.*\)/sudo ip6tables \1/' -e 's/\ -A / -C /')
		LOCK_ACTIVE="error while checking"
		# only checks for presence of rules, not for order; if not present in default table (filter), check in other tables
		MISSINGRULES="The following rules are not present:\n"
		for i in "${IPRULESIPV4[@]}"
		do
			eval "$i" &> /dev/null
			if [ ! $? = "0" ]
			then
				eval "${i/tables/tables -t nat}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				eval "${i/tables/tables -t mangle}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				eval "${i/tables/tables -t raw}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				eval "${i/tables/tables -t security}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				MISSINGRULES="$MISSINGRULES\nIPv4: $i"
				LOCK_ACTIVE="inactive"
			fi
		done
		for i in "${IPRULESIPV6[@]}"
		do
			eval "$i" &> /dev/null
			if [ ! $? = "0" ]
			then
				eval "${i/tables/tables -t nat}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				eval "${i/tables/tables -t mangle}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				eval "${i/tables/tables -t raw}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				eval "${i/tables/tables -t security}" &> /dev/null
			fi
			if [ ! $? = "0" ]
			then
				MISSINGRULES="$MISSINGRULES\nIPv6: $i"
				LOCK_ACTIVE="inactive"
			fi
		done
		if [ "$LOCK_ACTIVE" = "inactive" ]
		then
			MISSINGRULES="${MISSINGRULES//sudo iptables -C /}\n\nPlease check manually."
			MISSINGRULES="${MISSINGRULES//sudo ip6tables -C /}"
		else
			LOCK_ACTIVE="active"
		fi
	elif [ "$NETFILTER" = "nft" ]
	then
		# only checks if named tables from netfilter config file are present
		NFT_LOCK_TABLES=$( sudo nft list ruleset | grep "_lock" | wc -l )
		if [ "$NFT_LOCK_TABLES" -ge "3" ]
		then
			LOCK_ACTIVE="active"
		else
			LOCK_ACTIVE="inactive"
			MISSINGRULES="The default network lock is deactivated. The nft tables with rules for the default network lock are not loaded."
		fi
	else
		return 1
	fi
}

function yesno {
dialog \
	--backtitle "$BACKTITLE" \
	--title "$1" \
	--clear \
	--yesno "$2" \
	$HEIGHT $WIDTH
EXIT_STATUS=$?
}

get_userinfo
# if currently connected by openconnect, set status to unknown (connection could have been established outside of this script)
pgrep -f "openconnect.*--" &> /dev/null
if [ $? = 0 ]
then
	U_CONNECTED="connected (openconnect)"
	U_SERVER_FULL="unknown"
	U_TIME="unknown"
fi

# set default message for network lock status, so password doesn't have to be entered when starting the script to check status
if [ "$NETFILTER" = "none" ]
then
	LOCK_ACTIVE="None of the supported network filters are available, so the default network lock cannot be used."
else
	LOCK_ACTIVE="Select option 8 to check lock status."
fi

while true; do
	exec 3>&1
	selection=$(dialog \
		--cr-wrap \
		--backtitle "$BACKTITLE" \
		--title "Main Menu" \
		--clear \
		--cancel-label "Quit" \
		--menu "This is a control script for VPN connections, primarily for AirVPN's Hummingbird client.\nThis script can be exited and re-entered without affecting a running connection.\n\nUser: $U_LOGIN\nDays Until Expiration: $U_EXP\n\nDefault Network Lock: $LOCK_ACTIVE\n\nStatus: $U_CONNECTED\nServer: $U_SERVER_FULL\nConnected Since: $U_TIME\n\nPlease select one of the following options:" $HEIGHT $WIDTH 9 \
		"0" "Connect to recommended server" \
		"1" "Connect to server of specific country" \
		"2" "Connect to specific server" \
		"3" "Connect to random server" \
		"4" "Set options for hummingbird" \
		"5" "Connect via Openconnect" \
		"6" "Disconnect" \
		"7" "Refresh user info" \
		"8" "Check default network lock status" \
		"9" "Toggle default network lock" \
		2>&1 1>&3)
	EXIT_STATUS=$?
	exec 3>&-
	case $EXIT_STATUS in
		$DIALOG_CANCEL|$DIALOG_ESC)
			yesno "Quit" "Exit script?"
			case $EXIT_STATUS in
				$DIALOG_CANCEL|$DIALOG_ESC)
					;;
				$DIALOG_OK)
					break
					;;
			esac
			;;
	esac
	case $selection in
		0 )
			PASS_PROMPT="Connecting to and disconnecting from AirVPN with hummingbird requires root privileges. Please enter your password:"
			check_sudo
			if [ $? = "0" ]
			then
				disconnect_server
				#get_list
				DOMAIN="vpn.airdns.org"
				INIT_EXIT="1"
				connect_server "earth"
				if [ ! "$INIT_EXIT" = "0" ]
				then
					count="1"
					for s in "${SERVERS_BEST_EU[@]}"
					do
						connect_server "$s"
						if [ "$INIT_EXIT" = "0" ]
						then
							break
						else
							(( count++ ))
						fi
						if [ "$count" -ge 5 -o "$count" -ge "$CON_ATTEMPTS" ]
						then
							if [ "$count" -ge "$CON_ATTEMPTS" ]
							then
								notify-send "AirVPN" "Connection unsuccessful after $count failed attempts."
							fi
							break
						fi
					done
				fi
				if [ ! "$INIT_EXIT" = "0" -a "$count" -lt "$CON_ATTEMPTS" ]
				then
					for s in "${SERVERS_BEST_REST[@]}"
					do
						connect_server "$s"
						if [ "$INIT_EXIT" = "0" ]
						then
							break
						else
							(( count++ ))
						fi
						if [ "$count" -ge "$CON_ATTEMPTS" ]
						then
							notify-send "AirVPN" "Connection unsuccessful after $count failed attempts."
							break
						fi
					done
				fi
			fi
			;;
		1 )
			if [ ! -s "/tmp/.airvpn_server_list.txt" ]
			then
				get_list
			fi
			while true
			do
				sort_list_countries
				IFS=$':\n'
				exec 3>&1
				COUNTRY_NAME=$(dialog \
					--backtitle "$BACKTITLE" \
					--title "Country List" \
					--colors \
					--no-collapse \
					--ok-label "Recommended server" \
					--extra-button \
					--extra-label "Random server" \
					--column-separator ":" \
					--menu "Choose a country from the list to connect to.\n\n\Zb              Country    Country Code\ZB" \
					30 50 31 $LIST_COUNTRIES 2>&1 1>&3)
				EXIT_STATUS=$?
				exec 3>&-
				IFS=$' \t\n'
				case $EXIT_STATUS in
					$DIALOG_CANCEL|$DIALOG_ESC)
						break
						;;
					$DIALOG_EXTRA)
						PASS_PROMPT="Connecting to and disconnecting from AirVPN with hummingbird requires root privileges. Please enter your password:"
						check_sudo
						if [ $? = "0" ]
						then
							sort_list_servers
							SELECTED_COUNTRY=$(printf -- '%s\n' "${LIST_SERVERS[@]}" | grep "$COUNTRY_NAME" | shuf -n1 | cut -d ";" -f 1 )
							disconnect_server
							DOMAIN="airvpn.org"
							connect_server "$SELECTED_COUNTRY"
							if [ "$INIT_EXIT" = "0" ]
							then
								break
							fi
						fi
						;;
					$DIALOG_OK)
						PASS_PROMPT="Connecting to and disconnecting from AirVPN with hummingbird requires root privileges. Please enter your password:"
						check_sudo
						if [ $? = "0" ]
						then
							SELECTED_COUNTRY=$(printf -- '%s\n' "${LIST_COUNTRIES[@]}" | grep "^$COUNTRY_NAME" | cut -d ":" -f 2 )
							disconnect_server
							DOMAIN="vpn.airdns.org"
							connect_server "$SELECTED_COUNTRY"
							if [ "$INIT_EXIT" = "0" ]
							then
								break
							fi
						fi
						;;
				esac
			done
			;;
		2 )
			while true; do
			exec 3>&1
			SERVER_SORT=$(dialog \
				--backtitle "$BACKTITLE" \
				--title "Sort Server List" \
				--no-collapse \
				--ok-label "Sort ascending" \
				--extra-button \
				--extra-label "Sort descending" \
				--menu "Please choose how you want to sort the server list." \
				14 0 7 \
				"1" "Name" \
				"2" "Country" \
				"3" "Location" \
				"4" "Continent" \
				"5" "Bandwidth" \
				"6" "Users" \
				"7" "Load" \
				2>&1 1>&3)
			EXIT_STATUS=$?
			exec 3>&-
			case $EXIT_STATUS in
				$DIALOG_CANCEL|$DIALOG_ESC)
					break
					;;
				$DIALOG_EXTRA)
					SERVER_SORT_OPTION="r"
					;;
				$DIALOG_OK)
					SERVER_SORT_OPTION=""
					;;
			esac
			if [ "$SERVER_SORT" = "5" -o "$SERVER_SORT" = "6" -o "$SERVER_SORT" = "7" ]
			then
				SERVER_NUM_OPTION="n"
			else
				SERVER_NUM_OPTION=""
			fi
			if [ ! -s "/tmp/.airvpn_server_list.txt" ]
			then
				get_list
			fi
			while true
			do
				sort_list_servers "-k$SERVER_SORT,$SERVER_SORT$SERVER_SORT_OPTION$SERVER_NUM_OPTION"
				IFS=$';\n'
				exec 3>&1
				SELECTED_SERVER=$(dialog \
					--backtitle "$BACKTITLE" \
					--title "Server List" \
					--colors \
					--no-collapse \
					--extra-button \
					--extra-label "Refresh List" \
					--column-separator ":" \
					--menu "Choose a server from the list to connect to it. (Press ESC to go back.)\n\n\Zb Name            Country        Location                Continent Bandwidth Users Load Health\ZB" \
					40 102 31 $LIST_SERVERS 2>&1 1>&3)
				EXIT_STATUS=$?
				exec 3>&-
				IFS=$' \t\n'
				case $EXIT_STATUS in
					$DIALOG_CANCEL)
						break 2
						;;
					$DIALOG_ESC)
						break
						;;
					$DIALOG_EXTRA)
						get_list
						;;
					$DIALOG_OK)
						PASS_PROMPT="Connecting to and disconnecting from AirVPN with hummingbird requires root privileges. Please enter your password:"
						check_sudo
						if [ $? = "0" ]
						then
							disconnect_server
							DOMAIN="airvpn.org"
							connect_server "$SELECTED_SERVER"
							if [ "$INIT_EXIT" = "0" ]
							then
								break 2
							fi
						fi
						;;
				esac
			done
			done
			;;
		3 )
			PASS_PROMPT="Connecting to and disconnecting from AirVPN with hummingbird requires root privileges. Please enter your password:"
			check_sudo
			if [ $? = "0" ]
			then
				disconnect_server
				if [ ! -s "/tmp/.airvpn_server_list.txt" ]
				then
					get_list
				fi
				INIT_EXIT="1"
				count="0"
				while [ ! "$INIT_EXIT" = "0" ]
				do
					i="0"
					while [ $i -le 20 ]
					do
						RAN_SERVER_NM=$( grep -E "servers\..+\.public_name" /tmp/.airvpn_server_list.txt | shuf -n1 | cut -d "." -f 2 )
						RAN_SERVER_HEALTH=$( grep "servers\.$RAN_SERVER_NM\.health" /tmp/.airvpn_server_list.txt | cut -d "=" -f 2 )
						if [ "$RAN_SERVER_HEALTH" = "ok" ]
						then
							RAN_SERVER=$( grep "servers\.$RAN_SERVER_NM\.public_name" /tmp/.airvpn_server_list.txt | cut -d "=" -f 2 )
							break
						fi
						(( i++ ))
					done
					if [ "$i" -eq 20 ]
					then
						break
					elif [ "$count" -ge "$CON_ATTEMPTS" ]
					then
						notify-send "AirVPN" "Connection unsuccessful after $count failed attempts."
						break
					fi
					DOMAIN="airvpn.org"
					connect_server "$RAN_SERVER"
					(( count++ ))
				done
			fi
			;;
		4 )
			exec 3>&1
			HUM_OPTIONS=$(dialog \
				--backtitle "$BACKTITLE" \
				--title "Set custom Hummingbird options" \
				--extra-button \
				--extra-label "Make options permanent" \
				--form "If you want to use custom options for hummingbird, you can enter them here.\nType them like you would in the command line, separated by a space (e. g. --proto tcp --ignore-dns-push).\nNote that the options --timeout, --network-lock and --server are already used and can't be set here.\nThese options will override the ones you might have set in configuration file and will only be used for connections you make until you close the script. You can make them permanent with the button below (navigate with <TAB>)." $HEIGHT $WIDTH 5 \
				"Options:" 5 1 "$HUM_OPTIONS" 5 10 50 100 \
				2>&1 1>&3)
			EXIT_STATUS=$?
			exec 3>&-
			case $EXIT_STATUS in
				$DIALOG_CANCEL|$DIALOG_ESC)
					;;
				$DIALOG_EXTRA)
					sed -i -e '/^HUM_OPTIONS/d' "$VPNCONTROL_CONFIG"
					echo "HUM_OPTIONS=\"$HUM_OPTIONS\"" >> "$VPNCONTROL_CONFIG"
					;;
				$DIALOG_OK)
					;;
			esac
			;;
		5 )
			exec 3>&1
			# adjust field lengths if necessary
			CONNECT_INFO=$(dialog \
				--backtitle "$BACKTITLE" \
				--title "VPN via openconnect" \
				--insecure \
				--mixedform "Please provide your login credentials to connect to a VPN via openconnect:\n(Leave unneeded fields blank and type options as in command line, separated by space.)" $HEIGHT $WIDTH 6 \
				"Server:" 1 1 "" 1 21 25 0 0 \
				"Group:" 2 1 "" 2 21 25 0 0 \
				"User:" 3 1 "" 3 21 25 0 0 \
				"Password:" 4 1 "" 4 21 25 0 1 \
				"Additional Options:" 5 1 "--no-dtls" 5 21 25 0 0 \
				2>&1 1>&3)
			EXIT_STATUS=$?
			exec 3>&-
			case $EXIT_STATUS in
				$DIALOG_CANCEL|$DIALOG_ESC)
					;;
				$DIALOG_OK)
					PASS_PROMPT="Establishing OpenVPN connections requires root privileges. Please enter your password:"
					check_sudo
					if [ $? = "0" ]
					then
						disconnect_server
						connect_openconnect
					fi
					;;
			esac
			;;
		6 )
			PASS_PROMPT="Disconnecting from AirVPN with hummingbird requires root privileges. Please enter your password:"
			check_sudo
			if [ $? = "0" ]
			then
				disconnect_server
				if [ "$KILLED" = "true" ]
				then
						get_userinfo
				else
						U_CONNECTED="error during disconnection"
						U_SERVER_FULL="--"
						U_TIME="--"
				fi
			fi
			;;
		7 )
			get_userinfo
			;;
		8 )
			if [ "$NETFILTER" = "none" ]
			then
				dialog --backtitle "$BACKTITLE" --title "Network Lock Not Available" --timeout 3 --msgbox "$LOCK_ACTIVE" 10 35
			else
				pgrep hummingbird &> /dev/null
				if [ $? = 0 ]
				then
					dialog --backtitle "$BACKTITLE" --title "Check Default Network Lock" --timeout 8 --msgbox "Default network lock can only be checked when hummingbird is not running since it has it's own network lock overriding the default one." 10 35
				else
					PASS_PROMPT="Checking network traffic rules requires root privileges. Please enter your password:"
					check_sudo
					check_lock
					if [ "$LOCK_ACTIVE" = "inactive" ]
					then
						dialog --backtitle "$BACKTITLE" --title "Default Network Lock Inactive" --msgbox "$MISSINGRULES" $HEIGHT $WIDTH
					elif [ "$LOCK_ACTIVE" = "active" ]
					then
						dialog --backtitle "$BACKTITLE" --title "Default Network Lock Active" --timeout 3 --msgbox "The default network lock is active." $HEIGHT $WIDTH
					else
						return 1
					fi
				fi
			fi
			;;
		9 )
			if [ "$NETFILTER" = "none" ]
			then
				dialog --backtitle "$BACKTITLE" --title "Network Lock Not Available" --timeout 3 --msgbox "$LOCK_ACTIVE" 10 35
			else
				pgrep hummingbird &> /dev/null
				if [ $? = 0 ]
				then
					dialog --backtitle "$BACKTITLE" --title "Toggle Network Lock" --timeout 3 --msgbox "You need to be disconnected to change network traffic rules." 10 35
				else
					check_lock
					if [ "$LOCK_ACTIVE" = "inactive" ]
					then
						yesno "Toggle Network Lock" "Are you sure you want to activate the default network lock and block all connections while not connected to (any) VPN?"
						case $EXIT_STATUS in
							$DIALOG_CANCEL|$DIALOG_ESC)
								;;
							$DIALOG_OK)
								PASS_PROMPT="Changing network traffic rules requires root privileges. Please enter your password:"
								check_sudo
								if [ $? = "0" ]
								then
									toggle_lock "activate"
								fi
								;;
						esac
					elif [ "$LOCK_ACTIVE" = "active" ]
					then
						yesno "Toggle Network Lock" "Are you sure you want to deactivate the default network lock and allow all connections, even when not connected to a VPN?"
						case $EXIT_STATUS in
							$DIALOG_CANCEL|$DIALOG_ESC)
								;;
							$DIALOG_OK)
								PASS_PROMPT="Changing network traffic rules requires root privileges. Please enter your password:"
								check_sudo
								if [ $? = "0" ]
								then
									toggle_lock "deactivate"
								fi
								;;
						esac
					else
						return 1
					fi
				fi
			fi
			;;
	esac
done

clear
