# VPNControl

## Introduction
This is a wrapper for AirVPN's Hummingbird client (written in bash) to be able to use it more easily and extensively in the linux command line like with a GUI (i. e. Eddie), but with less resources. Also some more features are included, like the option to lock down the system's network traffic while not connected to a VPN and the possibility to connect to another VPN via openconnect.

This project is not actively maintained, but I will update and improve it from time to time.


## Features
* graphical interface in the command line to connect to AirVPN with Hummingbird (no Eddie involved)
* runs in background, the interface can be closed/opened anytime without affecting the running connection
* possibility to connect to any server with just one ovpn configuration file
* easily connect to a random server, to a recommended server, to the recommended server of a specific country or to a specific server
* sortable list of all servers including info like used bandwidth, load and number of users
* possibility to connect to other VPNs with openconnect
* lock down system by default (permanently if you want), so even without AirVPN/Hummingbird running there won't be any unwanted network traffic
* automatically establish connection at boot (which can later be controlled via the interface)
* logging of Hummingbird's output (number of days to keep logs for can be adjusted)
* system notifications to let you know what happens in the background

## Some general notes
* The default network lock determines, like Hummingbird itself, if iptables, iptables-legacy or nftables is available on your system and will use the first one found in that list. You can overwrite that by specifying which one to use in the configuration file. Once activated, the lock will stay in place until manually deactivated, so no internet connection will be possible unless connected to AirVPN or other whitelisted VPNs. You can make the lock permanent (or rather activate at boot) by enabling that option in the configuration file. AirVPN's network lock overwrites the default network lock, so there will be no interference.
* **IMPORTANT:** If you have any frontend firewall for iptables/nftables running, you might want to disable that or read up on how it might interfere with rule changes you make directly via iptables/nft. The same thing applies if you use just Hummingbird itself. If you enable the default permanent network lock, it will write the lock rules at boot, most likely overwriting rules by `firewalld` or the like, but other enabled firewalls might interfere later. Also important: If you have SELinux and you want to use nftables for Hummingbird starting at boot, you have to create a SELinux exception for nft bcause otherwise it will be denied and Hummingbird starts without setting up its own lock, thus leaving you unprotected (AirVPN staff is aware of this issue). You can do that with audit2allow. Follow for example [this](https://access.redhat.com/articles/2191331) guide to troubleshoot the problem and fix it with the solution given by sealert.
* [**NOTE:** the following seems to apply only to the network lock via iptables. When using nftables, network traffic to the DNS server (either via the router or directly to the IP address) must be allowed to initiate an AirVPN connection. I yet have to figure out if there is a way to initiate a connection without generally allowing traffic to the DNS server.] Check your `/etc/resolv.conf` file while not running Hummingbird (because Hummingbird's network lock replaces that file temporarily) to make sure your router is not set as a nameserver (so no 192.168... address). Some routers will push themselves on that list by DHCP whenever you connect to their network. Since communication with the router is allowed in the lock rules, DNS requests will be handled by the router and sent to whatever DNS server is configured there even when network traffic should be blocked. There are ways to prevent that file from being changed by DHCP, best configure network manager for that if you use it.
* To connect to other VPNs, their IPs must be whitelisted and DNS requests for their domains must be allowed in the default network lock rules (`netfilter_iptables.rulesipv4/ipv6` and/or `netfilter_nftables.rules`). Only edit those files with the default network lock deactivated. The rules for airvpn.org can be copied and adjusted.
* You can set custom options for Hummingbird in the interface or the configuration file. All the possible options can be found in the Hummingbird manual or with `sudo hummingbird --help`. The `--timeout`, `--network-lock` and `--server` options cannot be set directly because they are already used in the script and are controlled by other options.
* Apart from `dialog` I tried to only use basic system tools. The scripts will check if everything needed is present, if not they will exit. At least `bash 4` is needed. The scripts rely mostly on `dialog`, `awk` and `curl` (and `iptables`/`nft` as described and `openconnect` if needed), so it should work on most systems. I wrote and tested this on Fedora 32 with Hummingbird 1.0.3.
* It should be possible to use any ovpn config file generated by the AirVPN's config generator. Even with the file for one specific server it should be possible to connect to any other server because the server override function is used here. I haven't tested that extensively though and just use the config file for earth.
* [**NOTE:** The following doesn't seem to occur anymore as of Hummingbird 1.1.0.] AirVPN's API seems to be a little unreliable sometimes as in not correctly reporting the connection status. Sometimes the API reports me not being connected although I am connected to an AirVPN server. This is no big deal, it just means that the connection status sometimes may be shown falsely as disconnected. If you have the default network lock activated, no traffic would be possible if you were actually disconnected.

## Installation
1. Make sure you have the prerequisites installed: `dialog`, `bash` >=4, `curl` and `awk`.
1. Copy the content of all the files to separate files on your computer, name them accordingly, and put them in the appropriate folders. It says where they belong in the beginning of the file contents. Make sure to change the ownership of the systemd unit file to `root:root` and give the scripts execute permissions. Alternatively clone or download this repository, `cd` into the directory where the repository has been cloned/downloaded to and enter the following commands:

        mkdir -p "$HOME/.vpncontrol/config" && mkdir "$HOME/.vpncontrol/logs"
        mv vpncontrol.conf "$HOME/.vpncontrol/config/"
        mv netfilter_* "$HOME/.vpncontrol/config/"
        mv VPNControl.sh "$HOME/.local/bin/"
        sudo mv airvpn_boot.sh "/usr/local/bin/"
        sudo chown root:root airvpn.service
        sudo mv airvpn.service "/etc/systemd/system/"

1. Generate a config file with AirVPN's OpenVPN Config Generator (I use the one for "Earth", but theoretically it should work with any) and put it in the config directory. Adjust the path name in the configuration file.
1. The script assumes you have all the configuration files in the folder `$HOME/.vpncontrol/config/`, logs in `$HOME/.vpncontrol/logs/` and the boot script in `/usr/local/bin/`. In the `airvpn_boot.sh` script you have to adjust the path for the source command (line 34) to point to the configuration file. If you want to use different locations, you have to change them in the configuration file and (for the boot script) in the systemd unit file. If you want to use a different location for the configuration file itself, you have the change the `VPNCONTROL_CONFIG` variable in the `VPNControl.sh` script and again the source path in the `airvpn_boot.sh` script.
1. The `VPNControl.sh` script is meant to be run as a regular user. If you want to run it as root, you have to for sure change the `VPNCONTROL_CONFIG` variable from the previous step. Otherwise it should be possible to run it as root without problems (except notifications), but I haven't tested it.
1. You need to insert your own API key in the configuration file. It can be found in your account under Client Area -> API. Without this, connections will still work, but user info and connection status in the main window will not be properly updated.
1. Enable the systemd unit with

        sudo systemctl daemon-reload
        sudo systemctl enable airvpn.service

1. If you use a setup where bash cannot be found at `/bin/bash`, you have to change the path in the unit accordingly. Also I ran into a problem where systemctl complained that it couldn't find the unit. I don't know what the cause was (pretty sure the ownership and permissions were right), but it worked after I duplicated another `.service` file already present in `/etc/systemd/system/` (with `sudo cp`) and then renamed it and exchanged the contents.
1. Disable firewall frontends if needed (e. g. `firewalld`) and if you want to use the default network lock. (Firewall daemons don't necessarily interfere, but have the ability to overwrite the lock any time.)
1. DONE! Now Hummingbird will try to establish a connection after boot. You can call the control script at any time with `VPNControl.sh` and disconnect/reconnect/do whatever. The script can be exited without affecting the running connection. 
